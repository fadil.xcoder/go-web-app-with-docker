package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load(".env")

	router := mux.NewRouter()
	router.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		userId := os.Getenv("USER_ID")
		response := map[string]string{
			"message": "Welcome " + userId,
		}
		json.NewEncoder(rw).Encode(response)
	})

	router.HandleFunc("/{name}", func(rw http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		name := vars["name"]
		var message string
		if name == "" {
			message = "Hello World"
		} else {
			message = "Hello " + name
		}
		response := map[string]string{
			"message": message,
		}
		json.NewEncoder(rw).Encode(response)
	})

	log.Println("Server is running!")
	fmt.Println(http.ListenAndServe(":8581", router))
}
